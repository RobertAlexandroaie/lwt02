package lwt.lab;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.entity.ContentType;

import lwt.lab.html.utils.HTMLUtils;

/**
 * Servlet implementation class TreeParams
 */
@WebServlet("/ThreeParams")
public class ThreeParams extends HttpServlet {
    public static final String PARAM1_NAME = "param1";
    public static final String PARAM2_NAME = "param2";
    public static final String PARAM3_NAME = "param3";
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ThreeParams() {
	super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	try {
	    String param1 = request.getParameter(PARAM1_NAME);
	    String param2 = request.getParameter(PARAM2_NAME);
	    String param3 = request.getParameter(PARAM3_NAME);
	    response.setContentType(ContentType.TEXT_HTML.toString());
	    String paragraph = "The request params are: param1=" + param1 + ", param2=" + param2 + ", param3=" + param3;
	    response.getWriter().append(HTMLUtils.createP(paragraph));
	} catch (NullPointerException e) {
	    log(e.getMessage() + e.getStackTrace());
	}
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	doGet(request, response);
    }

}
