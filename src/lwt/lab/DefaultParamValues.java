package lwt.lab;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.entity.ContentType;

import lwt.lab.html.utils.HTMLUtils;

/**
 * Servlet implementation class DefaultParamValues
 */
@WebServlet("/DefaultParamValues")
public class DefaultParamValues extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final String NAME_PARAM_LABEL = "name";
    private static final String SURNAME_PARAM_LABEL = "surname";
    private static final String EMAIL_PARAM_LABEL = "email";

    private static final String NO_NAME_WARNING = "No name";
    private static final String NO_SURNAME_WARNING = "No surname";
    private static final String NO_EMAIL_WARNING = "No email";

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DefaultParamValues() {
	super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	String nameValue = getParameterValueWithDefault(request, NAME_PARAM_LABEL, NO_NAME_WARNING);
	String surnameValue = getParameterValueWithDefault(request, SURNAME_PARAM_LABEL, NO_SURNAME_WARNING);
	String emailValue = getParameterValueWithDefault(request, EMAIL_PARAM_LABEL, NO_EMAIL_WARNING);

	String[][] data = new String[3][2];
	data[0][0] = NAME_PARAM_LABEL;
	data[0][1] = nameValue;
	data[1][0] = SURNAME_PARAM_LABEL;
	data[1][1] = surnameValue;
	data[2][0] = EMAIL_PARAM_LABEL;
	data[2][1] = emailValue;

	response.setContentType(ContentType.TEXT_HTML.toString());
	response.getWriter().append(HTMLUtils.createTable(data, null));
    }

    protected String getParameterValueWithDefault(HttpServletRequest request, String parameterLabel,
	    String defaultValue) {
	String parameter = getParameterValue(request, parameterLabel);
	String finalValue = parameter == null || "".equals(parameter) ? defaultValue : parameter;
	return finalValue;
    }

    protected String getParameterValue(HttpServletRequest request, String parameterLabel) {
	return request.getParameter(parameterLabel);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	doGet(request, response);
    }

}
