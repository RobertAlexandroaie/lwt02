package lwt.lab;

import javax.servlet.Servlet;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import lwt.lab.utils.FilterUtils;

/**
 * Servlet implementation class FilteredDefaultParamValues
 */
@WebServlet("/FilteredDefaultParamValues")
public class FilteredDefaultParamValues extends DefaultParamValues implements Servlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see DefaultParamValues#DefaultParamValues()
     */
    public FilteredDefaultParamValues() {
	super();
	// TODO Auto-generated constructor stub
    }

    /*
     * (non-Javadoc)
     * 
     * @see lwt.lab.DefaultParamValues#getParameterValue(javax.servlet.http.
     * HttpServletRequest, java.lang.String)
     */
    @Override
    protected String getParameterValue(HttpServletRequest request, String parameterLabel) {
	return FilterUtils.filter(super.getParameterValue(request, parameterLabel));
    }

}
