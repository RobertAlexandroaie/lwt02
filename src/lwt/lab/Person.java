package lwt.lab;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.entity.ContentType;

import lwt.lab.html.utils.HTMLUtils;

/**
 * Servlet implementation class Person
 */
@WebServlet("/Person")
public class Person extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Person() {
	super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	Map<String, String[]> parameterMap = request.getParameterMap();
	String[][] data = new String[parameterMap.size()][];
	int row, col;
	row = col = 0;
	for (String key : parameterMap.keySet()) {
	    String[] keyValues = parameterMap.get(key);
	    data[row] = new String[keyValues.length + 1];
	    data[row][col] = key;
	    for (String keyValue : keyValues) {
		data[row][++col] = keyValue;
	    }
	    row++;
	    col = 0;
	}
	response.setContentType(ContentType.TEXT_HTML.toString());
	response.getWriter().append(HTMLUtils.createTable(data, null));
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	doGet(request, response);
    }

}
