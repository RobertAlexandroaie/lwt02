package lwt.lab;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lwt.lab.html.utils.HTMLUtils;

/**
 * Servlet implementation class ErrorHandling
 */
@WebServlet("/ErrorHandling")
public class ErrorHandling extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final String NAME_MESSAGE = "No name inserted";
    private static final String SURNAME_MESSAGE = "No surname inserted";
    private static final String EMAIL_MESSAGE = "No email inserted";

    protected static final String NAME_PARAM_LABEL = "name";
    protected static final String SURNAME_PARAM_LABEL = "surname";
    protected static final String EMAIL_PARAM_LABEL = "email";

    protected final StringBuilder bodyBuilder = new StringBuilder();

    private boolean nameEmpty;
    private boolean surnameEmpty;
    private boolean emailEmpty;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ErrorHandling() {
	super();

	String form = generateForm();
	bodyBuilder.append(form);
    }

    protected String generateForm() {
	String form = HTMLUtils.createPersonForm(NAME_PARAM_LABEL, SURNAME_PARAM_LABEL, EMAIL_PARAM_LABEL,
		"ErrorHandling", "post");
	return form;
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	response.getWriter().append(HTMLUtils.titleBodyHTML("HTML Gen", bodyBuilder.toString()));
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	addInsertedHTML(request.getParameter(NAME_PARAM_LABEL));
	doGet(request, response);
    }

    protected void addInsertedHTML(String content) {
	bodyBuilder.append(content);
    }

}
